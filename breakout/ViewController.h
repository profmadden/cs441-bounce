//
//  ViewController.h
//  breakout
//
//  Created by Patrick Madden on 2/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;

@end

