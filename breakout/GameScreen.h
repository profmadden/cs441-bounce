//
//  GameScreen.h
//  breakout
//
//  Created by Patrick Madden on 2/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
	float dx, dy;  // Ball motion
}
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;

-(void)createPlayField;

@end
